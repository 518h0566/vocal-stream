import 'package:flutter/material.dart';

class VocabStream {
  // Stream colorStream;

  Stream<String> getVocab() async* {
    final List<String> vocab = [
      'hard', 'hide', 'math', 'run', 'new', 'test'
    ];

    yield* Stream.periodic(const Duration(seconds: 1), (int t) {
      int index = t % 6;
      return vocab[index];
    });
  }
}
