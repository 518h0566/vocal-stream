import 'package:flutter/material.dart';
import '../stream/color_stream.dart';

// class App extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return HomePageStream();
//   }
//
// }

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }

}

class HomePageState extends State<StatefulWidget> {

  VocabStream vocabStream = VocabStream();
  String vocab = 'Vocabulary Flash...';
  int liveCount = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Color',
      home: Scaffold(
        appBar: AppBar(title: const Text('Stream - V0.0.5'),),
        //backgroundColor: bgTextColor,
        body: Center (
            child: Container(
                child: Column(
                  children: [Text(processStreamText(vocab), style: const TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
                    Text('Live Count: ' + liveCount.toString()),],
                )
            )
        ),
        floatingActionButton: FloatingActionButton(
          child: const Text('C'),
          onPressed: () {
            changeVocab();
          },
        ),
      ),
    );
  }

  changeVocab() async {
    vocabStream.getVocab().listen((eventVocab) {
      setState(() {
        //print(eventVocab);
        vocab = eventVocab;
      });
    });
  }

  processStreamText(String text) {
    String log = text.toLowerCase();
    print(log);
    if (vocab != 'Vocabulary Flash...') {
      liveCount++;
    }
    return log;
  }
}
